let config = {
  onUploadProgress: progressEvent => {
    let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
    console.log(percentCompleted);
    // do whatever you like with the percentage complete
    // maybe dispatch an action that will update a progress bar or something
  }
}
function retornaData(){

    var objCpr = new Object();
    objCpr.TABELA = "CADLANCAMENTO";
    objCpr.CAMPOS = ["NUMEROLANCAMENTO","VALORDOCUMENTO","DATAVENCIMENTO"];
    var aWhere = [];
    var objWhere = new Object();
    objWhere.CAMPO = "TIPO";
    objWhere.OPERADOR = "=";
    objWhere.VALOR = 'P';
    objWhere.CONDICAO = "AND";
    aWhere.push(objWhere);

    objCpr.WHERE = aWhere;
    objData = JSON.stringify(objCpr);

    return axios.post('http://erp1.og1.inf.br:29092/post/select_tabela',objData,config)
        .then(response => (dataAnt.lancamentos = response.data.Result));

}

var dataAnt = {
  items: ''
//   lancamentos: '',
//   dataInicialVencimento: '',
//   dataFinalVencimento: '',
//   tipo: 'P',
//   options: [
//   { text: 'Pagar', value: 'P' },
//   { text: 'Receber', value: 'R' }
// ]
};

/*
  Declarando os componentes
*/


/*
  Registrando componentes
*/

/*
  Instanciando o Vue
*/


var app1 = new Vue({
  el: '#app',
  data: {
          data: [],
          selected: null,
          tipoLancamento: '',
          dataInicial: new Date(),
          dataFinal: new Date(),
          columns: [
              {
                  field: 'NUMEROLANCAMENTO',
                  label: 'Título Número',
                  width: '40',
                  numeric: true
              },
              {
                  field: 'VALORDOCUMENTO',
                  label: 'Valor',
              },
              {
                  field: 'DATAVENCIMENTO',
                  label: 'Vencimento',
              }
          ]
      },
      mounted: function () {
        var objCpr = new Object();
        objCpr.TABELA = "CADLANCAMENTO";
        objCpr.CAMPOS = ["NUMEROLANCAMENTO","VALORDOCUMENTO","DATAVENCIMENTO"];
        var aWhere = [];
        var objWhere = new Object();
        objWhere.CAMPO = "TIPO";
        objWhere.OPERADOR = "=";
        objWhere.VALOR = 'P';
        objWhere.CONDICAO = "AND";
        aWhere.push(objWhere);

        objCpr.WHERE = aWhere;
        objData = JSON.stringify(objCpr);

        axios.post('http://erp1.og1.inf.br:29092/post/select_tabela',objData,config)
            .then(response => (this.data = response.data.Result));
      },
  methods: {
    listParceiros: function () {
      // res1 = retornaData();
      // //melhorar saporra
      // res1.then(function(result){ this.data = result});
      var objCpr = new Object();
      objCpr.TABELA = "CADLANCAMENTO";
      objCpr.CAMPOS = ["NUMEROLANCAMENTO","VALORDOCUMENTO","DATAVENCIMENTO"];
      var aWhere = [];
      var objWhere = new Object();
      objWhere.CAMPO = "TIPO";
      objWhere.OPERADOR = "=";
      objWhere.VALOR = this.tipoLancamento;
      objWhere.CONDICAO = "AND";
      aWhere.push(objWhere);
      console.log(aWhere);

      objCpr.WHERE = aWhere;
      objData = JSON.stringify(objCpr);

      axios.post('http://erp1.og1.inf.br:29092/post/select_tabela',objData,config)
          .then(response => (this.data = response.data.Result));
  }
}
});
